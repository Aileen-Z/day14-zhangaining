## O(Objective)
**Code review:** Finished revising and improving the code, and have a clearer understanding of how front-end data is stored and used, and how front-end and back-end data interact.
	
**Simulation project introduction:** The topic of the simulation project was introduced and the respective roles and division of labor were defined. The instructor gave further presentations and demonstrations on user stories, Kanban, and CI/CD. We also identified the general functions of the MVP.

**User Story:** Compiled user stories about the entire user booking and pickup process to see a movie and met with team members to assess the requirements to be done on Monday.

**Presentation:** Preparing for Monday's presentation.


## R(Reflective)
full

## I(Interpretive) 
In the process of organizing the requirements, many problems that may be encountered in communication were discovered, and some ill-considered designs were corrected in a timely manner.

## D(Decisional)
Communicate with everyone and do a good simulation project.