package com.example.demo.service.mapper;

import com.example.demo.entity.TodoItem;
import com.example.demo.service.dto.TodoRequest;
import com.example.demo.service.dto.TodoResponse;
import org.springframework.beans.BeanUtils;

public class TodoMapper {
    public static TodoItem toEntity(TodoRequest request) {
        TodoItem todoItem = new TodoItem();
        BeanUtils.copyProperties(request, todoItem);
        return todoItem;
    }

    public static TodoResponse toResponse(TodoItem todoItem) {
        TodoResponse todoResponse = new TodoResponse();
        BeanUtils.copyProperties(todoItem, todoResponse);
        return todoResponse;
    }

}
