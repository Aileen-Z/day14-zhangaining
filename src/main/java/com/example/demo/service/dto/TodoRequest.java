package com.example.demo.service.dto;

public class TodoRequest {
    private String name;
    private Boolean done;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public TodoRequest(String name, Boolean done) {
        this.name = name;
        this.done = done;
    }
}
