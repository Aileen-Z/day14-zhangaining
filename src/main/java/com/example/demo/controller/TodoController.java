package com.example.demo.controller;

import com.example.demo.entity.TodoItem;
import com.example.demo.service.TodoService;
import com.example.demo.service.dto.TodoRequest;
import com.example.demo.service.dto.TodoResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/todo")
public class TodoController {
    @Autowired
    TodoService todoService;

    @GetMapping
    public List<TodoItem> getAllTodos() {
        return todoService.findAll();
    }

    @PutMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public TodoResponse updateDone(@PathVariable Long id, @RequestBody TodoRequest todo) {
        return todoService.update(id, todo);
    }
}
