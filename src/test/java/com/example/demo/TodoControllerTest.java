package com.example.demo;

import com.example.demo.entity.TodoItem;
import com.example.demo.repository.TodoRepository;
import com.example.demo.service.dto.TodoRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
public class TodoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private TodoRepository todoRepository;
    @BeforeEach
    void setUp(){todoRepository.deleteAll();}
    @Test
    void should_find_todos() throws Exception {
        //
        TodoItem todoItem = new TodoItem(1L, "first to do item", true);
        todoRepository.save(todoItem);
        //
        mockMvc.perform(MockMvcRequestBuilders.get("/todo"))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath(("$[0].id")).value(todoItem.getId()))
                .andExpect(jsonPath(("$[0].name")).value(todoItem.getName()))
                .andExpect(jsonPath(("$[0].done")).value(todoItem.getDone()));

    }

    @Test
    void should_update_todo_done_when_use_updateTodo_given_only_done_changed() throws Exception {
        TodoItem todoItem = new TodoItem(1L, "first to do item", true);
        TodoItem actualTodo = todoRepository.save(todoItem);

        TodoRequest todoUpdateRequest = new TodoRequest("updated first to do item", false);
        ObjectMapper objectMapper = new ObjectMapper();
        String updatedTodoJson = objectMapper.writeValueAsString(todoUpdateRequest);
        mockMvc.perform(MockMvcRequestBuilders.put("/todo/{id}", actualTodo.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(updatedTodoJson))
                .andExpect(MockMvcResultMatchers.status().is(204));
        Optional<TodoItem> optionalTodoItem = todoRepository.findById(actualTodo.getId());
        assertTrue(optionalTodoItem.isPresent());
        TodoItem updatedTodo = optionalTodoItem.get();
        Assertions.assertEquals(todoUpdateRequest.getDone(), updatedTodo.getDone());
        Assertions.assertEquals(todoUpdateRequest.getName(), updatedTodo.getName());
    }

}
