package com.example.demo.service;

import com.example.demo.entity.TodoItem;
import com.example.demo.repository.TodoRepository;
import com.example.demo.service.dto.TodoRequest;
import com.example.demo.service.dto.TodoResponse;
import com.example.demo.service.mapper.TodoMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TodoService {
    TodoRepository todoRepository;

    public TodoService(TodoRepository todoRepository) {
        this.todoRepository = todoRepository;
    }

    public List<TodoItem> findAll() {
        return todoRepository.findAll();
    }

    public TodoItem findById(Long id) {
        return todoRepository.findById(id).get();
    }

//    public TodoItem updateDone(Long id, TodoItem todoItem) {
//        TodoItem todoItemToUpdate = this.findById(id);
//        todoItemToUpdate.setDone(!todoItem.getDone());
//        todoItemToUpdate.setName(todoItem.getName());
//        return todoItemToUpdate;
//    }

    public TodoResponse update(Long id, TodoRequest request) {
        TodoItem todoItemToUpdate = this.findById(id);
        TodoItem todoItem = TodoMapper.toEntity(request);
        todoItemToUpdate.setDone(todoItem.getDone());
        todoItemToUpdate.setName(todoItem.getName());
        return TodoMapper.toResponse(todoRepository.save(todoItemToUpdate));
    }

}
